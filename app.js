require("dotenv").config();
const express = require("express");
const mongoose = require("mongoose");
const app = express();

// importing routes
const iconRoutes = require("./routes/icon");

// db connection
mongoose
  .connect(process.env.Mongo_Uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("Database Connected....");
  })
  .catch((err) => {
    console.log(err);
  });

// middlewares
app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);

// routes
app.use("/api/v1", iconRoutes);
app.use("/api/v1/uploads", express.static("./uploads"));

app.get("/", (req, res) => {
  return res.json({
    message: "Welcome to pixler api.",
  });
});

app.listen(5000, () => {
  console.log("App is running on port 5000🚀");
});
