const express = require("express");
const {
  getIcons,
  getIconsById,
  addIcon,
  uploadImg,
  getIconsBySearch,
} = require("../controllers/icon");
const router = express.Router();

router.post("/icon", uploadImg, addIcon);
router.get("/icon", getIcons);
router.get("/icon/search", getIconsBySearch);
router.get("/icon/:id", getIconsById);

module.exports = router;
